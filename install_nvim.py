from utils import exec1, argDocString   
import os
    
argParser = argDocString("Install nvim and plugins")    
argParser.addArg("--install-nvim","Will install neovim")
argParser.addArg("--install-plugins", "Will install nvim plugins and custom configuration")
argParser.addArg("--install-nodejs", "Will install and build nodejs")

args = argParser.parseSysArgs()

HOME = os.getenv('HOME')
SOFTWARE = os.getenv('SOFTWARE',HOME+'/software')

if "--install-nvim" in args:
    exec1("wget https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz") 
    exec1("tar xzvf nvim-linux64.tar.gz")
    exec1("cp -r nvim-linux64/* "+SOFTWARE+"/")
    
if "--install-nodejs" in args:
    version = "16.15.1"
    exec1("wget https://nodejs.org/dist/v"+version+"/node-v"+version+"-linux-x64.tar.xz")
    exec1("tar xvf node-v"+version+"-linux-x64.tar.xz")
    exec1("cp -r node-v"+version+"-linux-x64/* "+SOFTWARE)
    #exec1("wget https://github.com/nodejs/node/archive/refs/tags/v21.6.1.tar.gz")   
    #exec1("tar xzvf v21.6.1.tar.gz")
    #with cd("node-21.6.1/"):    
    #    exec1("./configure")    
    #    exec1("make -j")    
    #    exec1("make install PREFIX="+SOFTWARE)

if "--install-plugins" in args:
    exec1("git clone https://gitlab.com/florian.feppon/vim_files.git")
    exec1("mkdir ~/.config/nvim")
    exec1("cp -r vim_files/* ~/.config/nvim/")
    exec1("pip install pynvim jedi --upgrade")
    exec1('nvim -c "PlugInstall | UpdateRemotePlugins | CocInstall coc-pyright"')
        
    

