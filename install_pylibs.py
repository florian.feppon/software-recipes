import sys
import os 
try:
    import utils
except:
   print("No installation of python-utilitaries ! Install it now")
   import subprocess
   import shutil 
   shutil.rmtree("utils", ignore_errors=True)
   subprocess.run("git clone https://gitlab.com/florian.feppon/python-utilitaries.git", shell=True, text=True) 
   subprocess.run("pip install numpy matplotlib psutil dill ipdb", shell=True, text=True) 
   shutil.move("python-utilitaries/utils",os.getcwd())
   shutil.rmtree("python-utilitaries")

from utils import argDocString, display, cd, exec1
HOME = os.getenv('HOME')
argParser = argDocString("Install LSMTO Python libraries")
argParser.addArg('--prefix',"Prefix location for python directories",meta="PREFIX",default=HOME+"/python")
argParser.addArg('--update',"Update python libraries")
argParser.addArg('--add-to-path',"Add libraries to python path")

args = argParser.parseSysArgs()
prefix = os.path.abspath(args['--prefix'])

display("Installing in "+prefix, color="orange_4a", attr="bold")

if '--update' in args:
    command = "pull"
else:
    command = "clone"

exec1("pip install scipy sympy colored numpy cvxopt osqp qpalm")
exec1("mkdir "+prefix)
if command == "clone":
    with cd(prefix):
        exec1(f"git {command} https://gitlab.com/florian.feppon/pymedit.git") 
        exec1(f"git {command} https://gitlab.com/florian.feppon/pyfreefem.git") 
        exec1(f"git {command} https://gitlab.com/florian.feppon/null-space-optimizer.git") 
        exec1(f"git {command} https://gitlab.com/florian.feppon/python-utilitaries.git") 
if command == "pull":   
    for repo in ['pymedit','pyfreefem','null-space-optimizer','python-utilitaries']:    
        with cd(prefix+"/"+repo):   
            exec1("git pull")

if '--add-to-path' in args:
    exec1(f"echo 'export PYTHONPATH=\"$PYTHONPATH:"+prefix+"/pymedit\"' >> ~/.bashrc")
    exec1(f"echo 'export PYTHONPATH=\"$PYTHONPATH:"+prefix+"/pyfreefem\"' >> ~/.bashrc")
    exec1(f"echo 'export PYTHONPATH=\"$PYTHONPATH:"+prefix+"/null-space-optimizer\"' >> ~/.bashrc")
    exec1(f"echo 'export PYTHONPATH=\"$PYTHONPATH:"+prefix+"/python-utilitaries\"' >> ~/.bashrc")
