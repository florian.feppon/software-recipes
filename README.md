# Software recipes  
    
This repository contains some Linux installers for the following software:  
        
- Python3 from the [Miniconda](https://docs.conda.io/projects/miniconda/en/latest/)
  distribution
- [Neovim](https://neovim.io/) as a text editor and my [personal
  configuration](https://gitlab.com/florian.feppon/vim_files)
- A minimal installation of [FreeFEM](https://freefem.org/) with
  [PETSc](https://petsc.org/release/)  
- [Paraview](https://www.paraview.org/) 
- [Mshdist](https://github.com/ISCDtoolbox/Mshdist),
  [Advect](https://github.com/ISCDtoolbox/Advection)  and  [Medit](https://github.com/ISCDtoolbox/Medit) from the 
  [ISCD toolbox](https://github.com/ISCDtoolbox)    
- the [Mmg](http://www.mmgtools.org/) library
- [ParMmg](https://github.com/MmgTools/ParMmg/), 
- Python libraries that I have been developing:
  [Pymedit](https://gitlab.com/florian.feppon/pymedit), [PyFreeFEM](https://gitlab.com/florian.feppon/pyfreefem)  and [Null Space Optimizer](https://gitlab.com/florian.feppon/null-space-optimizer)
    
The installers make use of additional Python functions available in [this
repository](https://gitlab.com/florian.feppon/python-utilitaries)   

## Procedure    
    
If you install this software from scratch, you should run the software in the following order:    
    
```bash     
sh install_python.sh    
python install_pylibs.py    
python install_nvim.py  --install-nvim --install-plugins
python install_iscd.py --install-mmg --install-commons \    
                --install-mshdist --install-advect
python install_freefem_linux.py --install-petsc --install-freefem
python install-iscd.py --install-parmmg
python install_paraview.py  
```
You can get information about the arguments accepted by these programs by calling them with the `-h` argument.
    
The software will be installed in the directory indicated by the    `$SOFTWARE` environment variable.  If this variable is not set,     it will be installed in `$HOME/software`. You need then to add  
```bash 
export PATH="$SOFTWARE/miniconda3/bin:$SOFTWARE/bin:$PATH"
```
to your `.bashrc` (where the miniconda3 path is required if you install Python with this installer).

