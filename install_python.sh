SOFTWARE=${SOFTWARE:-$HOME/software}
mkdir $SOFTWARE

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod u+x Miniconda3-latest-Linux-x86_64.sh
sh Miniconda3-latest-Linux-x86_64.sh -b -p "$SOFTWARE/miniconda3"

echo 'export PATH="$SOFTWARE/miniconda3/bin:$PATH"' >> ~/.bashrc
echo 'export PATH="$SOFTWARE/miniconda3/bin:$PATH"' >> ~/.bash_profile

