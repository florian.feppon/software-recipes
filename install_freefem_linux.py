import sys
import os 
try:
    import utils
except:
   print("No installation of python-utilitaries ! Install it now")
   import subprocess
   import shutil 
   shutil.rmtree("utils", ignore_errors=True)
   subprocess.run("git clone git@gitlab.com:ffeppon/python-utilitaries.git", shell=True, text=True) 
   subprocess.run("pip install numpy matplotlib psutil dill", shell=True, text=True) 
   shutil.move("python-utilitaries/utils",os.getcwd())
   shutil.rmtree("python-utilitaries")

from utils import argDocString, exec1, cd


argParser = argDocString("Install and compile FreeFEM with PETSc")
argParser.addArg('--install-petsc',"Will install and compile PETSc")
argParser.addArg('--install-freefem',"Will install and compile FreeFEM")
argParser.addArg('--petsc-configure',"Will only configure PETSc (real)")
argParser.addArg('--petsc-configure-complex',"Will only configure PETSc (complex)")
argParser.addArg('--petsc-compile',"Will only compile PETSc (real)")
argParser.addArg('--petsc-compile-complex',"Will only compile PETSc (complex)")
argParser.addArg('--update-freefem',"Will update and recompile FreeFEM")
argParser.addArg('--freefem-configure',"Will only run ./configure for FreeFEM")
argParser.addArg('--freefem-compile',"Will only compile (no ./configure) FreeFEM")
argParser.addArg('--install-prerequisites',"Will install prerequisites. Requires conda installed and bin/ folder from "
                                   " miniconda3/bin")
argParser.addArg("-j","Number of processors to use when calling make -j","J",default="")  

args = argParser.parseSysArgs()

if '--install-prerequisites' in args:
    exec1("conda install anaconda::flex")
    exec1("conda install conda-forge::bison")
    exec1("conda install anaconda::cmake")
    sys.exit(0)

if '--install-petsc' in args:
    args['--petsc-configure'] = True
    args['--petsc-configure-complex'] = True 
    args['--petsc-compile'] = True
    args['--petsc-compile-complex'] = True 

if '--install-freefem' in args:
    args['--freefem-configure'] = True
    args['--freefem-compile'] = True

if '--freefem-compile' in args or '--freefem-configure' in args:
    args['--install-freefem']=True
if '--petsc-configure' in args or '--petsc-configure-complex' in args:
    args['--install-petsc']=True
if '--petsc-compile' in args or '--petsc-compile-complex' in args:
    args['--install-petsc']=True

HOME = os.getenv('HOME')
SOFTWARE = os.getenv('SOFTWARE',HOME+'/software')
PREFIX = SOFTWARE
PWD = os.getcwd()

os.makedirs(PREFIX,exist_ok=True)

PETSC_SRC = PWD+"/petsc"
PETSC_PREFIX = PREFIX+"/ff-petsc/r"
PETSC_VAR = PETSC_PREFIX+"/lib/var/conf"
PETSC_PREFIX_C = PREFIX + "/ff-petsc/c"
PETSC_VAR_C = PETSC_PREFIX_C+"/lib/var/conf"

os.environ["PETSC_PREFIX"]= PETSC_PREFIX
os.environ["PETSC_VAR"]= PETSC_VAR
os.environ["PETSC_PREFIX_C"]=PETSC_PREFIX_C
os.environ["PETSC_VAR_C"]=PETSC_VAR_C

if '--install-petsc' in args:
    if not os.path.isdir('petsc'):
        exec1("git clone -b release https://gitlab.com/petsc/petsc.git petsc")

    os.makedirs(PETSC_SRC,exist_ok=True)
    with cd(PETSC_SRC):
        if '--petsc-configure' in args:
            exec1("git pull")
            cmd ="""/usr/bin/python3 configure   --download-mumps --download-parmetis --download-fblaslapack \
            --download-metis --download-hypre --download-superlu --download-slepc \
            --download-hpddm --download-ptscotch \
            --download-suitesparse \
            --download-scalapack \
            --download-tetgen \
            --with-fortran-bindings=no \
            --with-scalar-type=real \
            --with-debugging=no \
            --download-mpich \
            --download-mmg \
            --download-parmmg \
            --prefix="""+PETSC_PREFIX
            exec1(cmd)

        if '--petsc-compile' in args:
            exec1("make -j"+args['-j']+" all")
            exec1("make install")

        cmd="/usr/bin/python3 configure --with-mumps-dir=" +PETSC_PREFIX+" --with-parmetis-dir="+PETSC_PREFIX\
            +" --with-fblaslapack-dir="+PETSC_PREFIX \
            +" --with-metis-dir="+PETSC_PREFIX+" --with-hypre-dir="+PETSC_PREFIX+" --download-slepc "\
            +" --download-hpddm --with-ptscotch-dir="+PETSC_PREFIX \
            +" --with-suitesparse-dir="+PETSC_PREFIX \
            +" --with-scalapack-dir="+PETSC_PREFIX \
            +" --with-tetgen-dir="+PETSC_PREFIX \
            +" --with-superlu-dir="+PETSC_PREFIX \
            +" --download-htool "\
            +" --with-fortran-bindings=no "\
            +" --with-scalar-type=complex "\
            +" --with-debugging=no "\
            +" --with-mpi-dir="+PETSC_PREFIX \
            +" --with-mmg-dir="+PETSC_PREFIX \
            +" --with-parmmg-dir="+PETSC_PREFIX \
            +" --prefix="+PETSC_PREFIX_C

        if '--petsc-configure-complex' in args:
            exec1(cmd)

        if '--petsc-compile-complex' in args:
            exec1("make -j"+args['-j']+" all")
            exec1("make install")

if '--install-freefem' in args:
    FF_DIR = PWD+"/FreeFem-sources"
    os.environ["FF_DIR"]=FF_DIR
    if not os.path.isdir("FreeFem-sources"):
        exec1("git clone https://github.com/FreeFem/FreeFem-sources.git")
        exec1("git checkout origin/develop")
    os.environ["FCFLAGS"]="-fallow-argument-mismatch"
    #os.environ["FFLAGS"]="-fPIC"
    os.environ["CFLAGS"]="-fcommon -pthread"
    os.environ["CXXFLAGS"]="-fcommon -pthread"
    LD_LIBRARY_PATH = SOFTWARE+"/glut:"+os.getenv("LD_LIBRARY_PATH","")
    os.environ["LD_LIBRARY_PATH"]= LD_LIBRARY_PATH
    os.environ["LIBS"]="-pthread"
    MPIPREFIX=PREFIX+"/ff-petsc/r/bin"
    os.environ["MPIPREFIX"]=MPIPREFIX
    os.environ["MPICC"]=""+MPIPREFIX+"/mpicc"
    os.environ["MPICXX"]=""+MPIPREFIX+"/mpicxx"
    os.environ["MPIF77"]=""+MPIPREFIX+"/mpif77"
    os.environ["MPIFC"]=""+MPIPREFIX+"/mpif90"
    os.environ["MPIRUN"]=""+MPIPREFIX+"/mpirun"

    with cd("FreeFem-sources"):
        if '--freefem-configure' in args:
            exec1("git pull")
            exec1("autoreconf -i")
            exec1("./configure --without-hdf5 --enable-download --enable-optim "\
                  "--disable-ipopt --disable-nlopt --disable-yams --disable-mmg3d "\
                  "--disable-superlu --disable-gmm --disable-mumps-seq "\
                  "--prefix="+PREFIX+" --with-mpi=mpich --with-petsc="+PETSC_VAR\
                  +" --with-petsc-complex="+PETSC_VAR_C)

        if '--freefem-compile' in args:
            exec1("rm -rf ./3rdparty/pkg/*")
            exec1("make clean")

            exec1("./3rdparty/getall -a")
            exec1("make -j"+args['-j'])
            exec1("make install")
    

