import sys
import os 
from utils import argDocString, display, cd, exec1
try:
    from selenium import webdriver
except:
    exec1("pip install selenium")
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup

HOME = os.getenv('HOME')
SOFTWARE = os.getenv('SOFTWARE',HOME+'/software')
argParser = argDocString("Install Paraview")
argParser.addArg('-paraview_tar',"Specify Paraview archive if already downloaded","paraview_file.tar.gz")
argParser.addArg('-skip_download',"Skip downloading paraview file")
args = argParser.parseSysArgs()

if '-paraview_tar' in args:
    paraview_file = args['-paraview_tar'][0]
else:
    # Download ParaView
    # Load ParaView download page 
    chrome_options = Options()
    chrome_options.add_argument('--headless')  # Enable headless mode   
    chrome_options.add_argument('--disable-gpu')  # Disable GPU acceleration (optional)
    driver = webdriver.Chrome(options=chrome_options) 
    url = 'https://www.paraview.org/download/'
    driver.get(url)
    source = driver.page_source 
    driver.quit()
        
    soup = BeautifulSoup(source, 'html.parser')
    download_div = soup.find('div', class_='download-content')
    ParaViewUrl = download_div.find('a',class_= lambda t: t and t.startswith("ParaViewDownload"))['href']
        
    # Download ParaView
    if not '-skip_download' in args:
        exec1('wget "'+ParaViewUrl+'"')
    download_name = ParaViewUrl.split('/')[-1]  
    paraview_file = download_name.split('=')[-1]
    exec1('mv "'+download_name + '" '+paraview_file)
        
exec1("tar xzvf "+paraview_file)
folder ='.'.join(paraview_file.split('.')[:-2])
exec1("mv "+folder+" "+SOFTWARE)
cmd = "echo \'export PATH=\"$SOFTWARE/" + folder + "/bin:$PATH\"\'"
exec1(cmd + " >> ~/.bashrc")
exec1(cmd + " >> ~/.bash_profile")
    
desktop_entry = r"""    
[Desktop Entry]
Version=1.0
Name=ParaView
Comment=ParaView Scientific Visualization
Exec="""+SOFTWARE+"/"+folder+"/bin/paraview"+r"""
Icon="""+SOFTWARE+"/"+folder+"/share/icons/hicolor/96x96/apps/paraview.png"+r"""
Terminal=false
Type=Application
Categories=Education;Science;Development;
"""
shortcut_path = os.path.abspath("~/.local/share/applications/paraview.desktop")
with open(shortcut_path,"w") as f:    
    f.write(desktop_entry)
exec1("chmod u+x "+shortcut_path)
print("Created "+shortcut_path)


print("To uninstall paraview, just delete the folder "+SOFTWARE+"/"+folder+" and "+shortcut_path)
    
print("")   
print("Note: if paraview does not work and complains about xcb, do (on ubuntu)")    
print("sudo apt install libxcb-xinerama0 libxcb-xinput0")
