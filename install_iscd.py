import sys
import os 
try:
    import utils
except:
   print("No installation of python-utilitaries ! Install it now")
   import subprocess
   import shutil 
   shutil.rmtree("utils", ignore_errors=True)
   subprocess.run("git clone git@gitlab.com:ffeppon/python-utilitaries.git", shell=True, text=True) 
   subprocess.run("pip install numpy matplotlib psutil dill", shell=True, text=True) 
   shutil.move("python-utilitaries/utils",os.getcwd())
   shutil.rmtree("python-utilitaries")

from utils import argDocString, exec1, cd

argParser = argDocString("Install and compile software from the ISCD suite and MMg")
argParser.addArg('--install-commons','Will install the Commons ISCD library')
argParser.addArg('--install-mshdist','Will install Mshdist from ISCD library')
argParser.addArg('--install-advect','Will install Advect from ISCD library')
argParser.addArg('--install-medit','Will install Medit from ISCD library')
argParser.addArg('--install-mmg','Will install Mmg')
argParser.addArg('--install-parmmg','Will install ParMmg')
argParser.addArg('-b','Branch to checkout. If not specified, then will not checkout',meta="BRANCH")
argParser.addArg('-j','Will run make with NPROC',"NPROC",default="8")
argParser.addArg('-bear','Will run cmake with bear (required clang) to generate the compile_commands.json')
args = argParser.parseSysArgs()

if '-b' in args:    
    args['-b']=args['-b'][0]

HOME = os.getenv('HOME')
SOFTWARE = os.getenv('SOFTWARE',HOME+'/software')
PREFIX = SOFTWARE
PWD = os.getcwd()

os.makedirs(PREFIX,exist_ok=True)

if "-bear" in args: 
    bear = "bear -- "
else:
    bear = " "

if '--install-commons' in args:    
    if not os.path.isdir('Commons'):   
        exec1("git clone https://github.com/ISCDtoolbox/Commons.git")
    with cd("Commons"): 
        exec1("git pull")   
        exec1("rm -rf build")   
        exec1("mkdir build")    
        with cd("build"):   
            exec1("HOME="+SOFTWARE+" "+bear+" cmake ..")
            exec1("make -j "+ args['-j'])
            exec1("make install")
    

if '--install-mshdist' in args:    
    if not os.path.isdir('Mshdist'):   
        exec1("git clone https://github.com/ISCDtoolbox/Mshdist.git")
        exec1("patch Mshdist/CMakeLists.txt patches/cmake_mshdist.patch")
    with cd("Mshdist"): 
        exec1("unset DESTDIR")   
        exec1("rm -rf build")   
        exec1("mkdir build")    
        with cd("build"):   
            exec1("HOME="+SOFTWARE+" "+bear+" cmake ..")
            exec1("make -j "+args['-j'])
            exec1("make install")
    
if '--install-advect' in args:    
    if not os.path.isdir('Advection'):   
        exec1("git clone https://github.com/ISCDtoolbox/Advection.git")
    with cd("Advection"): 
        exec1("rm -rf build")   
        exec1("mkdir build")    
        with cd("build"):   
            exec1("HOME="+SOFTWARE+" "+bear+" cmake ..")
            exec1("make -j "+args['-j'])
            exec1("make install")
                

if '--install-medit' in args:    
    if not os.path.isdir('Medit'):   
        exec1("git clone https://github.com/ISCDtoolbox/Medit.git")
        exec1("patch Medit/CMakeLists.txt patches/patch_medit.patch")
    with cd("Medit"): 
        exec1("rm -rf build")   
        exec1("mkdir build")    
        with cd("build"):   
            exec1("HOME="+SOFTWARE+" "+bear+" cmake ..")
            exec1("make -j "+args['-j'])
            exec1("make install")
                
if '--install-mmg' in args: 
    if not os.path.isdir('mmg'):    
        exec1("git clone https://github.com/MmgTools/mmg.git")
    with cd("mmg"): 
        if '-b' in args:
            exec1(f"git checkout {args['-b']}")   
        exec1("git pull")   
        exec1("rm -rf build/*")
        exec1("mkdir -p build")
        with cd("build"):
            SCOTCH_DIR = SOFTWARE+"/ff-petsc/r/lib/"
            exec1(bear+" cmake "\
                  +" -DMMG_INSTALL_PRIVATE_HEADERS=ON "\
                  +" -DCMAKE_INSTALL_PREFIX="+SOFTWARE+" -DSCOTCH_DIR="+SCOTCH_DIR +" ..")
            exec1("make -j "+args['-j'])
            exec1("make install")

if '--install-parmmg' in args: 
    if not os.path.isdir('ParMmg'):    
        exec1("git clone https://github.com/MmgTools/ParMmg.git") 
    with cd("ParMmg"): 
        if '-b' in args:
            exec1(f"git checkout {args['-b']}")   
        exec1("git pull")   
        exec1("rm -rf build/*")
        exec1("mkdir -p build")
        MPIPREFIX=SOFTWARE+"/ff-petsc/r/bin"
        PETSC_INCLUDES = SOFTWARE+"/ff-petsc/r/include"
        MPICC=MPIPREFIX+"/mpicc"
        MPICXX=MPIPREFIX+"/mpicxx"
        MPIFC=MPIPREFIX+"/mpif90"
        MPIEXEC=MPIPREFIX+"/mpiexec"
        with cd("build"):
            SCOTCH_DIR = SOFTWARE+"/ff-petsc/r/lib/"
            PETSC_DIR = SOFTWARE+"/ff-petsc/r/"
            exec1(bear + " cmake -DDOWNLOAD_MMG=OFF"\
                  +" -DMMG_DIR=mmg " \
                  +" -DMMG_BUILDDIR=mmg/build "\
                  +" -DMPI_C_COMPILER="+MPICC \
                  +" -DMPI_CXX_COMPILER="+MPICXX \
                  +" -DMPI_Fortran_COMPILER="+MPIFC \
                  +" -DMPIEXEC_EXECUTABLE="+MPIEXEC
                  +" -DSCOTCH_DIR="+SCOTCH_DIR \
	              +" -DDOWNLOAD_METIS=OFF " \
                  +" -DMETIS_DIR="+PETSC_DIR \
                  +" -DCMAKE_INSTALL_PREFIX="+SOFTWARE+" ..")
            exec1("make -j "+args['-j'])
            exec1("make install")   

