# Show files with different permissions
find -type f -printf "%M %p\n" | awk '{ if(substr($1,2,1) != substr($1,5,1)) print $0 }'
# Change group to lsmto
chown -R :lsmto $LSMTO    
# Assign identical permissions from user to group
find . -exec chmod g=u {} +
# Remove writing permissions to the group and others 
find . -exec chmod -R go-w {} +
